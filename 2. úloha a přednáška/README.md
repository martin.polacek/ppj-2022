## Popiš JVM haldu a zásobník. Které proměnné jsou uložené kde?
- Dle paměťového modelu JVM
  - Halda - instance (objekty, pole, definice tříd)
    - Vytváření objektů na haldě se provádí pomocí klíčového slova new
    - Objekty není potřeba dealkokovat, to zajišťuje garbage collector
  - Zásobník - lokální proměnné (argumenty, lok. prom.)
    - Slouźí pro alokaci lokálních proměnných, ukládání parametrů a výsledků instrukcí JVM
    - Pamět je rozdělena po rámcích (frames)
    - Každé volaní metody alokuje nový rámec
    - Rámec obsahuje paměť pro lokální proměnné a tzv. zásobník operandů
    - JVM udržuje ukazatel na vrchol zásobníků, což je naposledy vložená hodnota

## Jak funguje garbage collector?

- Slouží pro uvolnění místa, pokud to aplikace potřebuje. Existuje Young a Old generace, kdy v Young jsou objekty, které byly v nejbližśí době alokovány. Do Old přecházejí, pokud určitý čas existují a jsou stále používány.
- Garbage collector vždy nejdříve hledá místo na uvolnění mezi Young objekty.

   ### Stop the world
   - Veškeré vlákna aplikace jsou pozastavena do té doby, dokud nejsou veškeré operace vykonány. Následně se všechná vlákna opět pustí.
   ### GC roots
   - Speciální objekty pro garbage collector
   - Slouží jako počátek prohledávání GC
   - VŠechny objekty, které mají přímou nebo nepřímou referenci na GC root tak nelze použít pro uvolnění paměti.
   - Několik typů:
     - Class: Třídy načtené pomocí systémového class loaderu
     - Stack local: lokální proměnné a parametry k metodám uložené v lokálním zásobníku
     - Active Java Threads: aktivní Java vlákna

## Popiš G1 collector
- Rozděluje haldu na regiony (až 2000)
- G1 nalezne vŠechny live objects současně (Aplikace není zastavena)
- Sleduje pointery mezi jednotlivými regiony
- Může tak uvolnit několik regionů naráz
- Vždy uvolňuje ty regiony, které obsahují nejvíce místa k uvolnění

## Popiš ZGC collector
- Nízká odezva (Maximálně 10 ms)
- Délka pauzy se neměni s velikostí haldy (vhodné pro serverové aplikace)
- V první fázi se vezmou všechny root reference (startovní body k dosažení objektů v haldě) - stop the world
- Následně se vytvoří graf od kořenových referencí
- Ve třetí fázi se řeší slabé reference - stop the world
- Každá reference se skládá z 64 bitů, kdy 42 bitů je použito na samotnou reprezentaci adresy
- Na vrchu reference jsou 4 bity (metabity), které ukládají stavy:
  - finalizable bit - objekt je dosažitelný jen pomocí finalizeru
  - remap bit - reference je up to date
  - markedo a marked1 bity - slouží pro označení dosažitelných objekt
- Následně projdeme jednotlivé bloky a zjistím, zda je chceme realokovat a pokud ano, dáme je do realokačního setu
- Pokud aplikace načte referenci, zapneme Load bariéru, která:
  - Zjistí zda remap bit je nastavený na 1, pokud ano, vrátí referenci
  - Zkontrolujeme, zda objekt je v realokačním setu, pokud ne, nechceme ho realokovat. Pro zamezení kontroly při dalším načtení, nastaví se remap bit na 1 a vrátíme referenci.
  - Nyní víme, že objekt, ke kterému chceme přistoupit je cílem realokace. Pokud již byl realokován, postupujeme k dalšímu kroku. Pokud ne, vytvoříme záznam ve forwarding table a pokračujeme dalším krokem.
  - Víme, že objekt byl realokován. Aktualizujeme referenci na novou lokaci objektu (zjistíme ve forwarding table) a nastavíme remap bit a vrátíme referenci

## Popiš bytecode (skupiny, prefix/suffix, typy operandů)

 - Jedná se o zdrojový kód pro JVM
 - Může být dále zkompilovaný do strojového kódu pro procesor
 - Skupiny:
   - Načtení a uložení (load, store)
   - Aritmetické a logické (add, cmpl)
   - Konverze datových typů (i2b, d2i)
   - Vytváření objektů a manipulace (new, putfield)
   - Správa zásobníku (swap, dup2)
   - Přesuny v kódu (goto)
  - Prefix slouźí pro označení datového typu parametru (např. i = integer)
  - Operandy určují s čím (jakými daty) bude operace provedena (konstatna, registr, adresa)

## Jak je generován bytecode a jak může být zobrazen?
- S příponou .class
- Generován překladačem javac
- Oproti strojovému kódu je čitelnější
- Lze zobrazit pomocí javap -v Main.class

## Popiš zásobník operandů a pole lokálních proměnných
- Na vrchol zásobníku ukládáme vždy hodnotu, se kterou chceme pracovat
- Pokud tedy zavoláme např. operaci iadd, která sečte dva integery, tak nejdříve pushneme do zásobníku dvě hodnoty a následně na vrchol umístíme jejich součet

## Popiš jak funguje interpretace bytecodu in runtime
- Dochází k parsování a vykonávání instrukcí jednu pro druhé
- Styl těch interperetů je velice jendoduchý na implementaci

## Co je JIT kompilace a jak funguje
- SLouží pro urychlení běhu programu
- Program je v době provádéní přeloźen přímo do strojového kódu, čímž dochází k urychlení běhu
- Problém je s dobou překladu, proto se využívá jen pokud je daná část kódu mnohokrát využívána.
- Málo času na provedení překladu. Výhodou je překlad přímo pro daný procesor.
