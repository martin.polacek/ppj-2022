package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.SemestralniPraceApplication;
import cz.tul.semestralni_prace.models.RestMessage;
import cz.tul.semestralni_prace.models.State;
import cz.tul.semestralni_prace.service.StateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/state")
public class RestStateReadController {
    @Autowired
    StateService stateService;

    private static final Logger LOGGER= LoggerFactory.getLogger(RestStateReadController.class);

    @GetMapping("/all")
    public ResponseEntity getAllStates() {
        try {
            return new ResponseEntity<List<State>>(stateService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

}
