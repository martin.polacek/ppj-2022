package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.SemestralniPraceApplication;
import cz.tul.semestralni_prace.models.*;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.StateDao;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import cz.tul.semestralni_prace.service.CityService;
import cz.tul.semestralni_prace.service.StateService;
import cz.tul.semestralni_prace.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/weather")
public class RestWeatherReadController {

    private static final Logger LOGGER= LoggerFactory.getLogger(RestWeatherReadController.class);

    @Autowired
    WeatherDataDao weatherDataDao;

    @Autowired
    CityDao cDao;

    @Autowired
    StateDao sDao;

    @Autowired
    WeatherService weatherService;

    @Autowired
    StateService stateService;

    @Autowired
    CityService cityService;

    @GetMapping("/avarageCity")
    public ResponseEntity avarageData(@RequestParam("city") Long city, @RequestParam("days") int days) {
        try {
            if(cityService.checkCityExist(city)) {
                return new ResponseEntity<AvarageWeatherData>(weatherService.avarageValues(city, days), HttpStatus.OK);
            }
            else {
                RestMessage message = new RestMessage(404, "city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
                    } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/avarageState")
    public ResponseEntity avarageData(@RequestParam("state") String state, @RequestParam("days") int days) {
        try {
            if(stateService.checkStateExist(state)) {
                return new ResponseEntity<AvarageWeatherData>(weatherService.avarageValues(state, days), HttpStatus.OK);
            }
            else {
                RestMessage message = new RestMessage(404, "state not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/actualData")
    public ResponseEntity actualData(@RequestParam("city") Long city) {
        try {
            if(!weatherService.checkDataExist(city)) {
                RestMessage message = new RestMessage(404, "record for city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<WeatherData>(weatherService.actualValue(city), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/export")
    public ResponseEntity export(Long city) {

        try {

            if(!weatherService.checkDataExist(city)) {
                RestMessage message = new RestMessage(404, "records for city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export.csv");

            String data = weatherService.csvDataForCity(city);

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(data.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(data);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(400, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

    }
}
