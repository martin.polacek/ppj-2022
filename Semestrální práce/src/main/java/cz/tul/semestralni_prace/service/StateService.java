package cz.tul.semestralni_prace.service;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.State;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.StateDao;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateService implements IStateService{

    @Autowired
    StateDao sDao;

    @Autowired
    CityDao cDao;

    @Autowired
    WeatherDataDao weatherDataDao;

    @Override
    public List<State> getAll() {
        return sDao.getAll();
    }

    @Override
    public State get(String code) {
        List<State> stateList = sDao.get(code);
        return stateList.get(0);
    }

    @Override
    public boolean insert(String city, String state) {
        State st = new State(city, state);
        sDao.insert(st);
        return true;
    }

    @Override
    public boolean delete(String code) {
        for(City city : cDao.get(code)) {

            cDao.delete(city.getId());
            weatherDataDao.removeValues(city.getId());
        }
        sDao.delete(code);
        return true;
    }

    @Override
    public boolean deleteAll() {
        sDao.deleteAll();
        return true;
    }

    @Override
    public boolean update(String code, String name) {
        sDao.update(code, name);
        return true;
    }

    @Override
    public boolean checkStateExist(String code) {
        List<State> stateList = sDao.get(code);
        if(stateList.isEmpty()) {
            return false;
        }
        return true;
    }
}
