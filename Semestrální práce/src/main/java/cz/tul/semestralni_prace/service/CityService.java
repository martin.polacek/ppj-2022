package cz.tul.semestralni_prace.service;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Service
public class CityService implements ICityService {

    @Autowired
    CityDao cDao;

    @Autowired
    WeatherDataDao weatherDataDao;

    @Override
    public List<City> getAll() {
        return cDao.getAll();
    }

    @Override
    public City get(Long id) {
        List<City> cList = cDao.get(id);
        return cList.get(0);
    }

    @Override
    public List<City> get(String stateCode) {
        return cDao.get(stateCode);
    }

    @Override
    public City get(String name, String stateCode) {
        List<City> cList = cDao.get(name, stateCode);
        return cList.get(0);
    }

    @Override
    public boolean insert(City city) {
        cDao.insert(city);
        return true;
    }

    @Override
    public boolean delete(Long id) {
        cDao.delete(id);
        weatherDataDao.removeValues(id);
        return true;
    }

    @Override
    public boolean deleteAll() {
        cDao.deleteAll();
        return true;
    }

    @Override
    public boolean update(Long id, String name) {
        cDao.update(id, name);
        return true;
    }

    @Override
    public boolean checkCityExist(String city, String state) {
        List<City> cityList = cDao.get(city, state);
        if(cityList.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean checkCityExist(Long id) {
        List<City> cityList = cDao.get(id);
        if(cityList.isEmpty()) {
            return false;
        }
        return true;
    }

    @Value("${openWeather.apiKey}")
    private String apiKey;

    @Value("${openWeather.URL}")
    private String apiURL;

    @Override
    public boolean validateCity(Long id, String city) throws IOException {
        List<City> cityList = cDao.get(id);
        return validate(city,cityList.get(0).getState());
    }

    @Override
    public boolean validateCity(String city, String state) throws IOException {
        return validate(city, state);
    }

    private boolean validate(String city, String state) throws IOException {
        String urls = String.format(apiURL, city, state, apiKey);
        URL url = new URL(urls);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        int code = connection.getResponseCode();
        if(code != 200) {
            return false;
        }
        return true;
    }
}
