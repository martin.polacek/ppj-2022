package cz.tul.semestralni_prace.service;

import cz.tul.semestralni_prace.models.City;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public interface ICityService {

    List<City> getAll();
    City get(Long id);
    List<City> get(String stateCode);
    City get(String name, String stateCode);
    boolean insert(City c);
    boolean delete(Long id);
    boolean deleteAll();
    boolean update(Long id, String name);
    boolean checkCityExist(String city, String state);
    boolean checkCityExist(Long id);
    boolean validateCity(Long id, String city) throws IOException;

    boolean validateCity(String city, String state) throws IOException;
}
