package cz.tul.semestralni_prace.repository;

import cz.tul.semestralni_prace.models.AvarageWeatherData;
import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.WeatherData;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class WeatherDataDao {

    @Autowired
    MongoTemplate mongoTemp;

    public WeatherData insert(WeatherData data) {

        return mongoTemp.save(data);
    }

    public AvarageWeatherData avarageValues(Long cityId, Date dateBefore) {

        MatchOperation filterDate = match(Criteria.where("city").is(cityId).and("time").gte(dateBefore).lt(new Date()));
        GroupOperation groupByValue = group("city").avg("temp").as("temp")
                .avg("pressure").as("pressure")
                .avg("humidity").as("humidity")
                .avg("visibility").as("visibility")
                .avg("wind_speed").as("wind_speed")
                .avg("wind_deg").as("wind_deg")
                .avg("clouds").as("clouds");

        return avarageValues(filterDate, groupByValue, cityId);
    }

    public AvarageWeatherData avarageValues(ArrayList<Long> citiesId, Date dateBefore) {
        MatchOperation filterDate = match(Criteria.where("city").in(citiesId).and("time").gte(dateBefore).lt(new Date()));
        GroupOperation groupByValue = group().avg("temp").as("temp")
                .avg("pressure").as("pressure")
                .avg("humidity").as("humidity")
                .avg("visibility").as("visibility")
                .avg("wind_speed").as("wind_speed")
                .avg("wind_deg").as("wind_deg")
                .avg("clouds").as("clouds");
        return avarageValues(filterDate, groupByValue, 404L);
    }

    private AvarageWeatherData avarageValues(MatchOperation filter, GroupOperation group, Long cityId) {

        Aggregation aggregation = newAggregation(filter, group);
        AggregationResults<Document> result = mongoTemp.aggregate(aggregation, "weatherData", Document.class);
        Document document = result.getUniqueMappedResult();

        double temp = document.getDouble("temp");
        double pressure = document.getDouble("pressure");
        double humidity = document.getDouble("humidity");
        double visibility = document.getDouble("visibility");
        double wind_speed = document.getDouble("wind_speed");
        double win_deg = document.getDouble("wind_deg");
        double clouds = document.getDouble("clouds");
        AvarageWeatherData data = new AvarageWeatherData(cityId, temp,pressure,humidity,visibility,wind_speed,win_deg,clouds);
        return data;
    }

    public boolean removeValues(Long city) {
        Query query = new Query(Criteria.where("city").is(city));
        mongoTemp.findAllAndRemove(query, WeatherData.class);
        return true;
    }

    public WeatherData actualValue(Long city) {
        Query query = new Query(Criteria.where("city").is(city));
        query.limit(1);
        query.with(Sort.by(Sort.Direction.DESC, "time"));

        WeatherData data = mongoTemp.findOne(query, WeatherData.class);
        return data;
    }

    public List<WeatherData> dataForCity(Long city) {
        Query query = new Query(Criteria.where("city").is(city));
        List<WeatherData> data = mongoTemp.find(query,WeatherData.class);
        return data;
    }
}
