package cz.tul.semestralni_prace.repository;

import cz.tul.semestralni_prace.models.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StateDao {
    @Autowired
    private JdbcTemplate jdbc;

    public int insert(State state) {
        return jdbc.update("INSERT INTO state (code, name) VALUES(?,?)", new Object[] {state.getCode(), state.getName()});
    }

    public List<State> getAll() {
        List<State> stateList = jdbc.query("SELECT code, name FROM state", (state, rowNum) ->
                new State(state.getString("code"), state.getString("name")));
        return stateList;
    }

    public List<State> get(String code) {
        List<String> params = new ArrayList<String>();
        params.add(code);
        List<State> cityList = jdbc.query("SELECT code,name FROM state WHERE code = ?", new Object[]{code}, (state, rowNum) ->
                new State(state.getString("code"), state.getString("name")));
        return cityList;
    }

    public int delete(String code) {
        return jdbc.update("DELETE FROM state WHERE code = ?", code);
    }

    public int deleteAll() {
        return jdbc.update("DELETE FROM state");
    }

    public State update(String code, String name) {
        jdbc.update("UPDATE state SET name = ? WHERE code = ?",name,code);
        State st = jdbc.queryForObject("SELECT code, name FROM state WHERE code = ?", (state, rowNum) ->
                new State(state.getString("code"), state.getString("name")),new Object[] {code});
        return st;
    }
}
