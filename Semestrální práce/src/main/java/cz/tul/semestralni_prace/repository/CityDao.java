package cz.tul.semestralni_prace.repository;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CityDao {

    @Autowired
    private JdbcTemplate jdbc;

    public int insert(City city) {
        return jdbc.update("INSERT INTO city (name, state) VALUES(?, ?)", new Object[] {city.getName(), city.getState()});
    }

    public List<City> getAll() {
        List<City> cityList = jdbc.query("SELECT id, name, state FROM city", (city, rowNum) ->
                new City(city.getLong("id"), city.getString("name"), city.getString("state")));
        return cityList;
    }

    public List<City> get(Long id) {
        List<City> cityList = jdbc.query("SELECT id, name, state FROM city WHERE id = ?", new Object[]{id}, (city, rowNum) ->
                new City(city.getLong("id"), city.getString("name"), city.getString("state")));
        return cityList;
    }

    public List<City> get(String stateCode) {
        List<City> cityList = jdbc.query("SELECT id, name, state FROM city WHERE state = ?", new Object[]{stateCode}, (city, rowNum) ->
                new City(city.getLong("id"), city.getString("name"), city.getString("state")));
        return cityList;
    }

    public List<City> get(String name, String stateCode) {
        List<City> cityList = jdbc.query("SELECT id, name, state FROM city WHERE state = ? AND name = ?", new Object[]{stateCode, name}, (city, rowNum) ->
                new City(city.getLong("id"), city.getString("name"), city.getString("state")));
        return cityList;
    }

    public int delete(Long id) {
        return jdbc.update("DELETE FROM city WHERE id = ?",id);
    }

    public int deleteAll() {
        return jdbc.update("DELETE FROM city");
    }
    public int update(Long id, String name) {
        return jdbc.update("UPDATE city SET name = ? WHERE id = ?",name,id);
    }
}
