package cz.tul.semestralni_prace.configs;

import com.mongodb.client.MongoClient;
import cz.tul.semestralni_prace.models.WeatherData;
import cz.tul.semestralni_prace.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class MongoConfig {
    @Autowired
    MongoClient mongoClient;

    @Value("${spring.data.mongodb.database}")
    private String db;

    @Value("${openWeather.expiration}")
    private Long expTime;

    @Bean
    MongoTemplate mongoTemplate() {
        MongoTemplate temp = new MongoTemplate(mongoClient, db);
        temp.indexOps(WeatherData.class).dropAllIndexes();
        temp.indexOps(WeatherData.class).ensureIndex(new Index().on("time", Sort.Direction.ASC).expire(expTime));
        temp.indexOps(WeatherData.class).ensureIndex(new Index().on("city", Sort.Direction.ASC));
        temp.indexOps(WeatherData.class).ensureIndex(new Index().on("state", Sort.Direction.ASC));
        return temp;
    }


}
