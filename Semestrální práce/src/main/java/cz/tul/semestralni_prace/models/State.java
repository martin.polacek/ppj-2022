package cz.tul.semestralni_prace.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "state")
public class State {

    @Id
    private String code;

    private String name;

    public State(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public State() {

    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }
}
