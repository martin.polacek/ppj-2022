package cz.tul.semestralni_prace.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TimeSeries;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



@Document(collection = "weatherData")
//@TimeSeries(collection = "weatherCollection", timeField = "time")
public class WeatherData {

    @Id
    String id;

    //@Indexed(expireAfter = "${openWeather.expiration}")
    Instant time;

    @Indexed private long city;
    @Indexed private String state;
    private float temp;
    private float pressure;
    private float humidity;
    private float visibility;
    private float wind_speed;
    private float wind_deg;
    private float clouds;

    public WeatherData(Long city, String state, float temp, float pressure, float humidity, float visibility, float wind_speed, float wind_deg, float clouds) {
        this.city = city;
        this.time = Instant.now();
        this.state = state;
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.visibility = visibility;
        this.wind_speed = wind_speed;
        this.wind_deg = wind_deg;
        this.clouds = clouds;
    }



    @Override
    public String toString() {
        return "WeatherData{" +
                "id='" + id + '\'' +
                ", time=" + time +
                ", city=" + city +
                ", state='" + state + '\'' +
                ", temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", visibility=" + visibility +
                ", wind_speed=" + wind_speed +
                ", wind_deg=" + wind_deg +
                ", clouds=" + clouds +
                '}';
    }

    public Instant getTime() {
        return time;
    }

    public long getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public float getTemp() {
        return temp;
    }

    public float getPressure() {
        return pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getVisibility() {
        return visibility;
    }

    public float getWind_speed() {
        return wind_speed;
    }

    public float getWind_deg() {
        return wind_deg;
    }

    public float getClouds() {
        return clouds;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    private String formatCSV() {
        return time + "," +
                city + "," +
                state + "," +
                temp + "," +
                pressure + "," +
                humidity + "," +
                visibility + "," +
                wind_speed + "," +
                wind_deg + "," +
                clouds;
    }

    public static String generateCSV(List<WeatherData> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("time,city,state,temp,pressure,humidity,visibility,wind_speed,wind_deg,clouds");
        for(WeatherData data : list) {
            sb.append(System.getProperty("line.separator"));
            String line = data.formatCSV();
            sb.append(line);
        }
        return sb.toString();
    }

    public static List<WeatherData> parseCSV(String csv) {
        String[] lines = csv.split(System.getProperty("line.separator"));
        List<WeatherData> dataList = new ArrayList<WeatherData>();
        for(int i = 1; i < lines.length; i++) {
            String line = lines[i];
            String[] data = line.split(",");
            if(data.length != 10) {
                continue;
            }
            Instant time = Instant.parse(data[0]);
            Long city = Long.parseLong(data[1]);
            String state = data[2];
            float temp = Float.parseFloat(data[3]);
            float pressure = Float.parseFloat(data[4]);
            float humidity = Float.parseFloat(data[5]);
            float visibility = Float.parseFloat(data[6]);
            float wind_speed = Float.parseFloat(data[7]);
            float wind_deg = Float.parseFloat(data[8]);
            float clouds = Float.parseFloat(data[9]);

            WeatherData wData = new WeatherData(city,state,temp,pressure,humidity,visibility,wind_speed,wind_deg,clouds);
            wData.time = time;
            dataList.add(wData);

        }
        return dataList;
    }

}
