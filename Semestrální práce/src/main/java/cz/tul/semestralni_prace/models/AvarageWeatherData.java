package cz.tul.semestralni_prace.models;

import org.springframework.data.mongodb.core.index.Indexed;


public class AvarageWeatherData {
    private long city;
    private double temp;
    private double pressure;
    private double humidity;
    private double visibility;
    private double wind_speed;
    private double wind_deg;
    private double clouds;

    public AvarageWeatherData(long city, double temp, double pressure, double humidity, double visibility, double wind_speed, double wind_deg, double clouds) {
        this.city = city;
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.visibility = visibility;
        this.wind_speed = wind_speed;
        this.wind_deg = wind_deg;
        this.clouds = clouds;
    }

    public long getCity() {
        return city;
    }

    public double getTemp() {
        return temp;
    }

    public double getPressure() {
        return pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getVisibility() {
        return visibility;
    }

    public double getWind_speed() {
        return wind_speed;
    }

    public double getWind_deg() {
        return wind_deg;
    }

    public double getClouds() {
        return clouds;
    }
}
