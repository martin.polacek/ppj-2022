package cz.tul.semestralni_prace;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.State;
import cz.tul.semestralni_prace.models.WeatherData;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.StateDao;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WeatherTest {

    @Autowired
    WeatherDataDao weatherDataDao;

    @Autowired
    StateDao sDao;

    @Autowired
    CityDao cDao;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void insertData() {

        weatherDataDao.removeValues(1L);
        weatherDataDao.removeValues(2L);
        sDao.deleteAll();
        cDao.deleteAll();

        State state1 = new State("CZ", "Czechia");
        State state2 = new State("DE", "Germany");
        sDao.insert(state1);
        sDao.insert(state2);

        City city1 = new City(1L, "Liberec", "CZ");
        City city2 = new City(2L, "Praha", "CZ");
        cDao.insert(city1);
        cDao.insert(city2);

        WeatherData data1 = new WeatherData(1L, "CZ",1,2,3,4,5,6,7);
        WeatherData data2 = new WeatherData(1L, "CZ",7,6,5,4,3,2,1);
        WeatherData data3 = new WeatherData(2L, "DE",2,3,4,5,6,7,8);
        weatherDataDao.insert(data1);

        Instant time = ZonedDateTime.now(ZoneOffset.UTC).minusDays(7).toInstant();
        data2.setTime(time);
        weatherDataDao.insert(data2);
        weatherDataDao.insert(data3);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void avarageValues_success() throws Exception {
        this.mockMvc.perform(get("/api/weather/avarageCity")
                        .param("city", "1")
                        .param("days", "1"))
                .andExpect(status().isOk());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void avarageValuesDays_success() throws Exception {
        this.mockMvc.perform(get("/api/weather/avarageCity")
                        .param("city", "1")
                        .param("days", "8"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"city\":1,\"temp\":4.0,\"pressure\":4.0,\"humidity\":4.0,\"visibility\":4.0,\"wind_speed\":4.0,\"wind_deg\":4.0,\"clouds\":4.0}")));
    }

    @Test
    void avarageValues_notFound() throws Exception {
        this.mockMvc.perform(get("/api/weather/avarageCity")
                        .param("city", "1000")
                        .param("days", "8"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void avarageValuesState_success() throws Exception {
        this.mockMvc.perform(get("/api/weather/avarageState")
                        .param("state", "CZ")
                        .param("days", "8"))
                .andExpect(status().isOk());
    }

    @Test
    void avarageValuesState_notFound() throws Exception {
        this.mockMvc.perform(get("/api/weather/avarageState")
                        .param("state", "CZ123321")
                        .param("days", "8"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void actualData_success() throws Exception {
        this.mockMvc.perform(get("/api/weather/actualData")
                        .param("city", "1"))
                .andExpect(status().isOk());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void actualData_notFound() throws Exception {
        this.mockMvc.perform(get("/api/weather/actualData")
                        .param("city", "1000"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void dataExport_success() throws Exception {
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders

                        .get("/api/weather/export")
                        .param("city", "1")
                        .contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
        assertEquals(200, result.getResponse().getStatus());
        assertEquals("application/octet-stream", result.getResponse().getContentType());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void dataExport_notFound() throws Exception {
        this.mockMvc.perform(get("/api/weather/export")
                        .param("city", "1000"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void dataImport_success() throws Exception {

        String csv_data = "time,city,state,temp,pressure,humidity,visibility,wind_speed,wind_deg,clouds" + System.getProperty("line.separator") +
                "2022-05-21T15:04:49.995Z,1,CZ,290.93,1016.0,55.0,10000.0,3.13,310.0,61.0";

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "import.csv", "multipart/form-data", csv_data.getBytes(StandardCharsets.UTF_8));
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/weather/import").file(mockMultipartFile).contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

}
