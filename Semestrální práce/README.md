# Semestrální projekt PPJ

## Databáze
- Jako relační databáze zvolena lokální MySQL
- Jako nerelační databáze zvolena MongoDB
  - Funguje online, není potřeba měnit nastavení v property souboru

## Nastavení aplikace

- profily:
  - prod (produkční)
  - readOnly (nelze upravovavat data v databázi, neprovadí se aktualizace)
  - test (připojení k testovacím DB)
- openWeather
  - expirace (za jak dlouho expiruje záznam o počasí)
  - updateTime (jak často požadujeme, aby se aktualizovali záznamy o počasí)
  - apiKey
  - URL
- Mongo
  - uri
  - database
- MySQL
  - url
  - username
  - password

## REST API

#### <span style="color:yellow">Výpis všech států (GET)
```
http://.../api/state/all
```

- Vrátí všechny státy uložené v databázi

#### <span style="color:yellow">Smazání státu (DELETE)
```
http://.../api/state/delete
```

- Smaže vybraný stát, všechny jeho města a jejich záznamy o počasí

###### Parametry
 - `code`: string

#### <span style="color:yellow">Vložení státu (POST)
```
http://.../api/state/insert
```

- Vloží stát do databáze, pokud již stát s daným kódem neexistuje

###### Parametry
 - `code`: string
 - `name`: string


#### <span style="color:yellow">Výpis všech měst (GET)
```
http://.../api/city/all
```

- Vrátí všechny města uložené v databázi

#### <span style="color:yellow">Smazání města (DELETE)
```
http://.../api/city/delete
```

- Smaže vybrané město a všechny jeho záznamy o počasí

###### Parametry
 - `id`: Long

#### <span style="color:yellow">Vložení města (POST)
```
http://.../api/city/insert
```

- Vloží města do databáze, pokud již město s daným názvem neexistuje

###### Parametry
 - `city`: string
 - `state`: string

#### <span style="color:yellow">Změna názvu města (PUT)
```
http://.../api/state/update
```

- Změní název města, pokud je název validní

###### Parametry
 - `id`: Long
 - `name`: string

#### <span style="color:yellow">Průměrné hodnoty pro město za určitý čas (GET)
```
http://.../api/weather/avarageCity
```

- Získá průměrné hodnoty počasí pro dané město za určitý počet dní

###### Parametry
 - `city`: Long
 - `days`: int

#### <span style="color:yellow">Průměrné hodnoty pro stát za určitý čas (GET)
```
http://.../api/weather/avarageState
```

- Získá průměrné hodnoty počasí pro daný stát za určitý počet dní

###### Parametry
 - `code`: string
 - `days`: int

#### <span style="color:yellow">Aktuální hodnota pro město (GET)
```
http://.../api/weather/actualData
```

- Získá aktuální hodnoty počasí pro dané město

###### Parametry
 - `city`: Long

#### <span style="color:yellow">Export dat pro město (GET)
```
http://.../api/weather/actualData
```

- Získá CSV soubor všech dat o počasí pro dané město

###### Parametry
 - `city`: Long

#### <span style="color:yellow">Import dat pro město (POST)
```
http://.../api/weather/actualData
```

- Nahraje do databáze všechny data ze souboru pro město, které existuje

###### Parametry
 - `file`: MultipartFile